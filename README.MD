## Task manager

#### Требования к SOFTWARE
 Наличие предустановленной [Java Oracle](https://www.oracle.com/technetwork/java/javase/downloads/index.html) на ПК.
 Java 8
#### Использованные технологии
 При написании Task Manager использовалась Java 1.8.152_release, Apache Maven 3.6.0.
 Разработка велась в IDE IntellijIDEA 2018.3.1
#### Разработчик
 Ветошников Виталий Дмитриевич
#### Контакты
 email: vit.vetoshnikov@gmail.com
#### Сборка приложения
 Для сборки приложения используется [Apache Maven](https://maven.apache.org/download.cgi)
 mvn clean
 mvn install
#### Запуск приложения
 Put .war archive to webapp directory of Tomcat