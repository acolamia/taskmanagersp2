package ru.iteco.vetoshnikov.taskmanager.api;

import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {
    List<ProjectDTO> allProjects();

    void add(ProjectDTO projectDTO);

    void delete(ProjectDTO projectDTO);

    void edit(ProjectDTO projectDTO);

    ProjectDTO getById(String id);
}
