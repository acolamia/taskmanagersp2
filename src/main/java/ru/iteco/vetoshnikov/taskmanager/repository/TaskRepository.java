package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.model.Task;

import java.util.List;


@Repository
public interface TaskRepository extends JpaRepository<Task, String> {
    @Query(value = "SELECT a FROM Task a WHERE a.project.id=:projectId")
    List<Task> findAllByProjectId(@Param("projectId") @NotNull final String projectId);

    @Query(value = "SELECT a FROM Task a WHERE (a.id=:id AND a.project.id=:projectId)")
    Task getByProjectIdAndId(@Param("id") @NotNull final String id, @Param("projectId") @NotNull final String projectId);

    @Modifying
    @Query(value = "DELETE FROM Task a WHERE a.project.id=:projectId")
    void deleteAllByProjectId(@Param("projectId") @NotNull final String projectId);

//    @NotNull
//    private final static Map<String, Task> taskMap = new HashMap<>();
//
//    @Nullable
//    @Override
//    public List<Task> allTasks(@NotNull final String projectId) {
//        @NotNull final List<Task> taskList = new ArrayList<>();
//        for (@Nullable final Task task : taskMap.values()) {
//            if (task.getProjectId() == null || task.getProjectId().isEmpty()) continue;
//            if (projectId.equals(task.getProjectId())) {
//                taskList.add(task);
//            }
//        }
//        return taskList;
//    }
//
//    @Override
//    public void add(@NotNull final Task task) {
//        taskMap.put(task.getId(), task);
//    }
//
//    @Override
//    public void delete(@NotNull final Task task) {
//        taskMap.remove(task.getId());
//    }
//
//
//    @Override
//    public void edit(@NotNull final Task task) {
//        taskMap.put(task.getId(), task);
//    }
//
//    @Nullable
//    @Override
//    public Task getById(@NotNull final String id) {
//        return taskMap.get(id);
//    }
}
