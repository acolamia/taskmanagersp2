package ru.iteco.vetoshnikov.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskRepository;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.constant.StatusType;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.model.Project;
import ru.iteco.vetoshnikov.taskmanager.api.IProjectService;

import java.util.List;

@Controller
public class ProjectController {
    @Autowired
    @NotNull
    private IProjectService projectService;
    @NotNull
    @Autowired
    private ITaskService taskService;

    @GetMapping(value = "/projectListPage")
    public ModelAndView allProjects() {
        @NotNull final List<ProjectDTO> projectList = projectService.allProjects();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("projectListPage");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @GetMapping(value = "/editProject/{id}")
    public ModelAndView editProjectPage(@PathVariable("id") @NotNull final String id) {
        @NotNull final ProjectDTO project = projectService.getById(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("projectEditPage");
        modelAndView.addObject("statusList", StatusType.values());
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @PostMapping(value = "/editProject")
    public ModelAndView editProject(@ModelAttribute("project") @NotNull final ProjectDTO project) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projectListPage");
        projectService.edit(project);
        return modelAndView;
    }

    @GetMapping(value = "/addProject")
    public ModelAndView addProjectPage() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("projectAddPage");
        modelAndView.addObject("statusList", StatusType.values());
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @PostMapping(value = "/addProject")
    public ModelAndView addProject(@ModelAttribute("project") @NotNull final ProjectDTO project) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projectListPage");
        projectService.add(project);
        return modelAndView;
    }

    @GetMapping(value = "/deleteProject/{id}")
    public ModelAndView deleteProject(@PathVariable("id") @NotNull final String id) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projectListPage");
        @NotNull final ProjectDTO project = projectService.getById(id);
        taskService.deleteAllByProjectId(id);
        projectService.delete(project);
        return modelAndView;
    }
}
